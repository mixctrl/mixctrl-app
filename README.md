# MixctrlApp

Local Development
Twitter: mix_ctrl
https://twitter.com/mix_ctrl
https://docs.ghost.org/install/ubuntu/
https://app.mixctrl.io/

## Pipelines

-   Build
-   Lint
-   Deploy

https://about.gitlab.com/2016/04/07/gitlab-pages-setup/
https://gitlab.com/help/user/project/pages/getting_started_part_two.md#create-a-project-from-scratch

### Setup

https://juristr.com/blog/2018/02/cd-gitlab-angular-firebase/

## Documentation

http://127.0.0.1:8080/

https://firebase.google.com/
[Mixctrl Documentation](https://mixctrl.gitlab.io/mixctrl-app/)
https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx

## Licence

MixCtrl @2019
